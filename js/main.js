gnr={};

googleService = {
    initMap : function(){
        var myLatLng =  {lat: 19.432632, lng: -99.133237};

        gnr.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: myLatLng,
            disableDefaultUI: true,
            gestureHandling: 'none',
            zoomControl: false
        });

        google.maps.event.addListener(gnr.map, 'click', function( event ){
            googleService.colocaMarker(event.latLng.lat()+","+event.latLng.lng());
        });
    },
    colocaMarker: function (coordenadas){
        if (gnr.trafficLayer == null) {
            gnr.trafficLayer = new google.maps.TrafficLayer();
            gnr.trafficLayer.setMap(gnr.map);
        }
        coordenadas = coordenadas.split(",");

        if (gnr.marker != null) {
            gnr.marker.setMap(null);
        }

        gnr.map.setCenter({lat: parseFloat(coordenadas[0]), lng: parseFloat(coordenadas[1])});
        //gnr.map.setZoom(16);
        
        gnr.marker = new google.maps.Marker({
            position: {lat: parseFloat(coordenadas[0]), lng: parseFloat(coordenadas[1])},
            map: gnr.map
        });

    },
    colocaMarkerSelect:function(){
        var select = document.getElementById('soflow');
        var data = select.value.split('|');
        if (data.length > 1) {
                
            var coordenadas = data[0].split(',');

            if (gnr.trafficLayer == null) {
                gnr.trafficLayer = new google.maps.TrafficLayer();
                gnr.trafficLayer.setMap(gnr.map);
            }

            if (gnr.marker != null) {
                gnr.marker.setMap(null);
            }

            gnr.map.setCenter({lat: parseFloat(coordenadas[0]), lng: parseFloat(coordenadas[1])});
            //gnr.map.setZoom(16);
            
            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<div id="bodyContent" style="text-align:left;">'+
                '<p>Cliente: '+data[1]+'</p>'+
                '<p>Proveedor: '+data[2]+'</p>'+
                '<p>Ubicaci&oacute;n: '+data[3]+'</p>'+
                '</div>'+
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });


            gnr.marker = new google.maps.Marker({
                position: {lat: parseFloat(coordenadas[0]), lng: parseFloat(coordenadas[1])},
                map: gnr.map
            });

            gnr.marker.addListener('click', function() {
                infowindow.open(gnr.map, gnr.marker);
            });
        }
    }
}

Ajax = {
    enviaAjax : function(url, parametros){
        var variableFuncion = "constructor";   
        $.ajax({
            type: "POST",
            url: url,
            data: '',
            success: function(rsl){
              variableFuncion[variableFuncion][variableFuncion]( rsl )(); 
            }
        });
    },
}