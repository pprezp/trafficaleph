<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Aleph Traffic Shaper</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <script src="js/main.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaVGEZmTx_8d8O4VmOt6lIW5_FeyH7fSk&callback=googleService.initMap"></script>
</head>
<body>
    <div id="header" class="IntfHead" >
        <div id="logo"><img src="img/aleph.jpg" style="max-width:100%"/></div>
    </div>
    <div class="Body-page">
        <div class="Body-page-Sec1">
            <span>Selecciona un lugar de la lista: </span>
            <select id="soflow" style="font-size: 15px;" onchange="googleService.colocaMarkerSelect()">
                <option selected="selected">Da click aqui para seleccionar algun lugar de la lista</option>
            <?php
                include 'lib/clases/MySQL.php';
                $objMysql = new MySQL();
                echo $objMysql->dataSelect();
            ?>
            </select>
        </div>
        <div id="places" class="Body-page-Sec2">
            <table class="Table">
                <thead>
                    <tr><th>Color de la v&iacute;a</th><th>Significado</th></tr>
                </thead>
                <tbody style="text-align:left;">
                    <tr><td>Verde</td><td>M&aacute;s de 80Km por hora</td></tr>
                    <tr><td>Amarillo</td><td>De 40 a 80Km por hora</td></tr>
                    <tr><td>Rojo</td><td>Menos de 40Km por hora</td></tr>
                    <tr><td>Rojo / Negro</td><td>Muy lento o con autos detenidos</td></tr>
                    <tr><td>Gris</td><td>Sin informaci&oacute;n</td></tr>
                </tbody>
            </table>
        </div>
        <div id="map" class="Body-page-Sec2"></div>
    </div>
    
    <div id="load" style="height: 350px;padding: 1px; display:none">
        <div id="loader" class='Loader' style='margin: 50px auto;'></div>
            <span id="span_loader" class='Lbl_Loader'>Cargando Registros...</span>
        </div>
    </div>
</body>
</html>