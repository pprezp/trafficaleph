<?php
    class MySQL{
        private $db = null;

		public function __construct($user = 'root', $pass = 'Pablo1.', $dataBase = 'epicenterdbusa') {
			$this->db = new mysqli('localhost', $user, $pass, $dataBase);
			if (mysqli_connect_error()) {
				die('Error de Conexión (' . $this->db->connect_errno . ') ' . $this->db->connect_error);
			}
		}

		public function sqlCRUD($query){
			$this->db->autocommit(FALSE);
			try{
				$this->db->query($query);
				$this->db->commit();
			}catch(Exception $error){				
				$this->db->rollback();
			}
		}
		
		public function dataSelect(){
			$resultado = $this->db->query('SELECT * FROM ubicaciones');
			if ($resultado->num_rows > 0) {
				while ($fila = $resultado->fetch_array()) {
					extract($fila);
					$js .= "<option value=\"$coordenadas|$nombreCliente|$proveedor|$ubicacion\">$ubicacion</option>";
				}
			}
			return $js;
		}
        
        public function __destruct(){
			if(!empty($this->db)){
				$this->db->close();
			}
		}
    }
?>